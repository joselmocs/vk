# -*- coding: utf-8 -*-

from threading import Thread
from Queue import Queue
import argparse
import logging
import os

from user import User
from terminal import T


p = argparse.ArgumentParser(description="tool for download albums from vk.com")
p.add_argument('-vk', help='id of vk.com album separated by comma', required=True)
p.add_argument('-t', '--threads', help='quantity of threads used for download images', default=1, required=False, type=int)
p.add_argument('-d', '--data', help='path of downloaded albums', default="/tmp/vk", required=False)


class Vk:
    url = "https://vk.com"
    finished = False
    terminated = False

    def __init__(self, config):
        self.config = config
        self.headers = {
            "referer": self.url,
            "origin": self.url,
            "user-agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.130 Safari/537.36"
        }

    @property
    def data(self):
        return self.config.data

    def download(self):
        self.prepare()

        for vk in self.config.vk.split(","):
            if self.terminated:
                continue

            self.user = User(vk.strip(), self)
            self.user.download()

        self.finished = True

    def prepare(self):
        self.q = Queue()
        for i in range(self.config.threads):
            t = Thread(target=self.worker)
            t.daemon = True
            t.start()

        if not os.path.isdir(self.data):
            os.mkdir(self.data)
            os.chmod(self.data, 0777)

    def worker(self):
        while not self.finished and not self.terminated:
            self.q.get().download()
            self.q.task_done()

    def terminate(self):
        self.terminated = True
        T.write("exiting...")
        self.q.join()


if __name__ == '__main__':
    vk = Vk(p.parse_args())
    try:
        vk.download()
    except KeyboardInterrupt:
        vk.terminate()
    except Exception as ex:
        vk.terminate()
        print(ex)
