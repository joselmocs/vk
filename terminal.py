# -*- coding: utf-8 -*-
import sys
import time
import itertools
import threading
from datetime import datetime


class T:
    BOLD = '\033[1m'
    END = '\033[0m'
    ERASE_LINE = '\033[K'
    NEW_LINE = '\n'
    LINE_START = '\r'

    spinner_circle = itertools.cycle(['-', '/', '|', '\\'])
    spinner_delay = 0.1
    spinner_busy = False
    date_format = "%H:%M:%S"
    last_text = '_'

    @staticmethod
    def now():
        return datetime.now().strftime(T.date_format)

    @staticmethod
    def bold(text):
        return '%s%s%s' % (T.BOLD, text, T.END)

    @staticmethod
    def write(text):
        if T.spinner_busy:
            T.spinner_stop()

        sys.stdout.write(text + T.NEW_LINE)
        sys.stdout.flush()

    @staticmethod
    def wait(text):
        T.last_text = text
        if not T.spinner_busy:
            T.spinner_busy = True
            threading.Thread(target=T.spinner_task).start()

    @staticmethod
    def freeze():
        sys.stdout.write(T.LINE_START + T.ERASE_LINE + T.last_text + T.NEW_LINE)
        sys.stdout.flush()

    @staticmethod
    def spinner_stop():
        T.spinner_busy = False
        time.sleep(T.spinner_delay)
        T.freeze()

    @staticmethod
    def spinner_task():
        while T.spinner_busy:
            sys.stdout.write(T.LINE_START + T.ERASE_LINE + '%s %s' % (T.last_text, T.spinner_circle.next()))
            sys.stdout.flush()
            time.sleep(T.spinner_delay)
