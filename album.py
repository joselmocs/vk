# -*- coding: utf-8 -*-
from BeautifulSoup import BeautifulSoup
import requests
import logging
import time
import os

from terminal import T
from photo import Photo

total_downloaded = 0
total_failed = 0


class Album:
    def __init__(self, user, id, name, index):
        self.user = user
        self.id = id
        self.name = name if name else id
        self.index = index
        self.photos = []
        self.total_images = 0

    @property
    def q(self):
        return self.user.q

    @property
    def url(self):
        return "https://vk.com/album-%s_%s" % (self.user.id, self.id)

    @property
    def data(self):
        return "%s/%s" % (self.user.data, self.name)

    @property
    def terminated(self):
        return self.user.terminated

    def failed(self):
        global total_failed
        total_failed += 1
        self.info()

    def success(self):
        global total_downloaded
        total_downloaded += 1
        self.info()

    def info(self):
        global total_downloaded
        global total_failed

        T.wait("vk.com/%s %s,  album %s of %s,  downloading %s of %s%s" % (
            self.user.user,
            T.bold(self.name),
            T.bold(self.index),
            T.bold(self.user.total_albums),
            T.bold(total_downloaded),
            T.bold(self.total_images),
            ",  failed: %s" % T.bold(total_failed) if total_failed > 0 else "",
        ))

    def download(self):
        if self.terminated:
            return

        self.fetch()
        self.prepare()

        if not len(self.photos):
            T.wait("vk.com/%s %s,  album %s of %s,  no new images" % (
                self.user.user,
                T.bold(self.name),
                T.bold(self.index),
                T.bold(self.user.total_albums)
            ))
            return

        global total_downloaded
        global total_failed
        total_downloaded = 0
        total_failed = 0

        T.wait("vk.com/%s %s,  album %s of %s,  downloading %s of %s" % (
            self.user.user,
            T.bold(self.name),
            T.bold(self.index),
            T.bold(self.user.total_albums),
            T.bold(total_downloaded),
            T.bold(self.total_images)
        ))

        for photo in self.photos:
            self.q.put(photo)
        self.q.join()

        T.wait("vk.com/%s %s,  album %s of %s,  downloaded: %s%s" % (
            self.user.user,
            T.bold(self.name),
            T.bold(self.index),
            T.bold(self.user.total_albums),
            T.bold(total_downloaded),
            ",  failed: %s" % T.bold(total_failed) if total_failed > 0 else "",
        ))
        T.freeze()

    def prepare(self):
        if len(self.photos) and not os.path.isdir(self.data):
            os.mkdir(self.data)
            os.chmod(self.data, 0777)

    def fetch(self):
        T.wait("vk.com/%s %s,  album %s of %s,  new images found: %s" % (
            self.user.user,
            T.bold(self.name),
            T.bold(self.index),
            T.bold(self.user.total_albums),
            T.bold(0)
        ))

        offset = 0
        ended = False

        while not ended and not self.terminated:
            try:
                request = requests.post(
                    self.url, {
                        "al": 1,
                        "offset": offset,
                        "part": 1
                    },
                    headers=self.user.parent.headers,
                    allow_redirects=False
                )
            except Exception as ex:
                T.write("[%s] [%s] failed to fetch photos, trying again in 5 segs" % (self.user.name, self.name))
                time.sleep(5)
                continue

            try:
                clean = "<div%s" % request.content.split("<!><div")[1]
            except IndexError:
                ended = True
                continue

            parser = BeautifulSoup(clean)
            rows = parser.findAll('div', {"class": "photos_row "})

            for row in rows:
                id = row.attrMap.get("id").split("_")[-1]
                photo = Photo(
                    album=self,
                    id=id
                )
                if not photo.file_exists():
                    self.photos.append(photo)
                    self.total_images += 1

                    T.wait("vk.com/%s %s,  album %s of %s,  new images found: %s" % (
                        self.user.user,
                        T.bold(self.name),
                        T.bold(self.index),
                        T.bold(self.user.total_albums),
                        T.bold(self.total_images)
                    ))

            offset += len(rows)

            if not len(rows):
                ended = True
                continue
