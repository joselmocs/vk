import requests
from bs4 import BeautifulSoup


def filter_int_from_str(string):
    return int(''.join(x for x in string if x.isdigit()))


class User:
    def __init__(self, id, name, albums_count):
        self.id = id
        self.name = name
        self.albums_count = albums_count
        self.albums = []

    def to_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "albums_count": self.albums_count
        }

    def __str__(self):
        return "%s (%s albums)" % (self.name, self.albums_count)

    def __repr__(self):
        return "User: %s" % self.__str__()


class Album:
    def __init__(self, id, name, url, thumb, photos_count):
        self.id = id
        self.name = name
        self.url = url
        self.thumb = thumb
        self.photos_count = photos_count
        self.photos = []

    def to_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "url": self.url,
            "thumb": self.thumb,
            "photos_count": self.photos_count
        }

    def __str__(self):
        return "%s (%s photos)" % (self.name, self.photos_count)

    def __repr__(self):
        return "Album: %s" % self.__str__()


class Script:
    url = "https://m.vk.com/albums-34908971?offset=%s"

    def __init__(self):
        self.next_offset = 0
        self.headers = {
            "referer": self.url % str(self.next_offset),
            "origin": self.url % str(self.next_offset),
            "user-agent": "Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1"
        }

    def run(self):
        request = requests.get(
            self.url % str(self.next_offset),
            headers=self.headers,
            allow_redirects=False
        )

        if request.status_code != 200:
            raise RuntimeError("status code error: %s" % request.status_code)

        self.cookies = request.cookies

        parser = BeautifulSoup(request.content, features="html.parser")

        self.user = User(
            id=int(self.url.split("-")[1].split("?")[0]),
            name=parser.title.text,
            albums_count=filter_int_from_str(parser.find("h4", {"class": "sub_header"}).text)
        )

        bs_albums = parser.find_all("a", {"class": "album_item"})

        if not len(bs_albums):
            return

        for bs_album in bs_albums:
            if "album_item_top" in bs_album.attrs["class"]:
                continue

            album_url = bs_album.attrs["href"]

            self.user.albums.append(Album(
                id=int(album_url.split("_")[1]),
                name=bs_album.find("div", {"class": "album_name"}).text,
                url=album_url,
                thumb=bs_album.find("img", {"class": "album_thumb_img"}).attrs["src"],
                photos_count=int(bs_album.find("div", {"class": "album_cnt"}).text)
            ))


        breakpoint()




Script().run()
