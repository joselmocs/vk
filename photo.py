# -*- coding: utf-8 -*-
import requests
import logging
import json
import os

from terminal import T


class Photo:
    def __init__(self, album, id):
        self.album = album
        self.id = id
        self.url = None

    @property
    def log(self):
        return self.album.log

    @property
    def file_name(self):
        return "%s/%s.jpg" % (self.album.data, self.id)

    @property
    def terminated(self):
        return self.album.terminated

    def file_exists(self):
        return os.path.exists(self.file_name)

    def download(self):
        if self.terminated:
            return

        self.fetch()
        if self.download_photo():
            self.album.success()

    def fetch(self):
        try:
            request = requests.post(
                "https://vk.com/al_photos.php", {
                    "al": 1,
                    "act": "show",
                    "list": "album-%s_%s" % (self.album.user.id, self.album.id),
                    "module": "photos",
                    "photo": "-%s_%s" % (self.album.user.id, self.id)
                },
                headers=self.album.user.parent.headers,
                allow_redirects=False
            )
        except Exception as ex:
            T.write("fetch: %s" % (self.url, ex.message))
            self.album.failed()
            return

        if request.status_code != 200:
            self.album.failed()
            return

        clean = request.content.split("<!><!json>")[1]
        cleaned = json.loads(''.join([i if ord(i) < 128 else ' ' for i in clean]))

        b_letter = "a"
        resolution = 0
        src = None

        for item in cleaned:
            if item.get("id") != "-%s_%s" % (self.album.user.id, self.id):
                continue

            if "comments" in item:
                item["comments"] = None

            for letter in ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]:
                tmp = item.get("%s_" % letter)
                if tmp and isinstance(tmp, list) and len(tmp) == 3:
                    if tmp[1] > resolution:
                        resolution = tmp[1]
                        b_letter = letter
                else:
                    if resolution == 0 and item.get("%s_src" % letter):
                        src = item.get("%s_src" % letter)

            if resolution > 0:
                src = item.get("%s_src" % b_letter) or item.get("%s_src" % b_letter.upper())

        self.url = src

    def download_photo(self):
        if not self.url:
            return False

        try:
            request = requests.get(
                self.url,
                headers=self.album.user.parent.headers,
                stream=True
            )
        except Exception as ex:
            T.write("%s: %s" % (self.url, ex.message))
            self.album.failed()
            return False

        if request.status_code != 200:
            self.album.failed()
            return False

        with open(self.file_name, 'wb+') as f:
            for chunk in request.iter_content(1024):
                f.write(chunk)

        return True
