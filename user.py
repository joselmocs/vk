# -*- coding: utf-8 -*-
from BeautifulSoup import BeautifulSoup
from string import digits
import requests
import logging
import time
import re
import os

from terminal import T
from album import Album


class User:
    def __init__(self, user, parent):
        self.user = user
        self.parent = parent
        self.albums = []
        self.total_albums = 0
        self.total_images = 0

    @property
    def q(self):
        return self.parent.q

    @property
    def url(self):
        return "https://vk.com/%s" % self.user

    @property
    def data(self):
        return "%s/%s" % (self.parent.data, self.name)

    @property
    def terminated(self):
        return self.parent.terminated

    def download(self):
        T.write("vk.com/%s -----------------------" % self.user)
        T.write("vk.com/%s initialized at %s" % (
            self.user,
            T.bold(T.now())
        ))

        self.prepare()
        self.fetch()

        T.write("vk.com/%s -----------------------" % self.user)

        for album in self.albums:
            album.download()

        T.write("vk.com/%s finalized at %s" % (
            self.user,
            T.bold(T.now())
        ))

    def prepare(self):
        request = requests.get(
            self.url,
            headers=self.parent.headers,
            allow_redirects=False
        )

        parser = BeautifulSoup(request.content)
        self.id = parser.find(
            "div", {
                "class": "module clear album_module _module"
            }
        ).find(
            "a", {
                "class": "module_header"
            }
        ).attrMap["href"].split("-")[1]
        self.name = self.clean_name(parser.find("h2", {"class": "page_name"}).text, False) or self.id

        if not os.path.isdir(self.data):
            os.mkdir(self.data)
            os.chmod(self.data, 0777)

        T.wait("vk.com/%s %s,  %s albums,  %s images" % (
            self.user,
            T.bold(self.name),
            T.bold(self.total_albums),
            T.bold(self.total_images)
        ))

    def fetch(self):
        offset = 0
        photos = 0
        ended = False

        while not ended and not self.terminated:
            try:
                request = requests.post(
                    "https://vk.com/albums-%s" % self.id, {
                        "al": 1,
                        "offset": offset,
                        "part": 1
                    },
                    headers=self.parent.headers,
                    allow_redirects=False
                )
            except Exception as ex:
                T.write("[%s] failed to fetch albums, trying again in 5 segs" % ex.message)
                time.sleep(5)
                continue

            try:
                clean_content = "<div%s</div>" % request.content.split("<!><div")[1].split("</div><!>")[0]
            except IndexError:
                ended = True
                continue

            parser = BeautifulSoup(clean_content)
            rows = parser.findAll('div', {"class": "photo_row photos_album _photos_album"})

            for row in rows:
                count = row.find("div", {"class": "photos_album_counter fl_r"}).text
                if not count:
                    continue

                self.total_albums += 1
                self.total_images += int(count)

                id = row.attrMap.get("id").split("_")[1].strip()
                name = self.clean_name(row.find("div", {"class": "photos_album_title ge_photos_album"}).text) or id

                album = Album(
                    user=self,
                    id=id,
                    name=name,
                    index=self.total_albums
                )
                self.albums.append(album)

                T.wait("vk.com/%s %s,  %s albums,  %s images" % (
                    self.user,
                    T.bold(self.name),
                    T.bold(self.total_albums),
                    T.bold(self.total_images)
                ))

            offset += len(rows)

    def clean_name(self, name, numbers=True):
        name = re.sub("\W+", " ", name.strip()).strip()
        if not numbers:
            name = filter(lambda c: not c.isdigit(), name)

        return name.strip()
